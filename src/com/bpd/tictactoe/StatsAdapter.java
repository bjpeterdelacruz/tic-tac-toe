package com.bpd.tictactoe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bpd.tictactoe.BoardManager.Player;
import android.app.Activity;
import android.database.SQLException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class StatsAdapter extends BaseAdapter {

  private final Activity activity;
  private Map<String, List<String>> stats;
  private List<String> names = new ArrayList<String>();

  public StatsAdapter(Activity activity) {
    if (activity == null) {
      throw new IllegalArgumentException("activity is null");
    }
    this.activity = activity;
    updateStats(activity);
  }

  private void updateStats(final Activity activity) {
    new AsyncTask<Void, Void, Map<String, List<String>>>() {

      @Override
      protected Map<String, List<String>> doInBackground(Void... params) {
        DatabaseHelper helper = null;
        try {
          helper = new DatabaseHelper(activity);
          helper.openDatabase();
          return helper.getStatsForAllUsers();
        }
        catch (SQLException sqle) {
          Log.e(StatsAdapter.class.getCanonicalName(), sqle.getMessage(), sqle);
        }
        finally {
          if (helper != null) {
            helper.close();
          }
        }
        return new HashMap<String, List<String>>();
      }

      @Override
      protected void onPostExecute(Map<String, List<String>> results) {
        stats = new HashMap<String, List<String>>(results);
        names = new ArrayList<String>();
        for (String name : stats.keySet()) {
          names.add(name);
        }
        notifyDataSetChanged();
      }

    }.execute();
  }

  @Override
  public int getCount() {
    return names.size();
  }

  @Override
  public Object getItem(int position) {
    return stats.get(names.get(position));
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LinearLayout layout = new LinearLayout(activity);
    layout.setOrientation(LinearLayout.VERTICAL);

    String name = names.get(position);

    TextView nameTv = new TextView(activity);
    LinearLayout.LayoutParams params =
        new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    nameTv.setLayoutParams(params);
    nameTv.setText(name);
    nameTv.setGravity(Gravity.CENTER);
    nameTv.setTextColor(Color.WHITE);
    nameTv.setBackgroundColor(Constants.COLORS.get(Player.PLAYER));
    nameTv.setTextSize(Constants.STATS_LIST_TEXT_SIZE);

    layout.addView(nameTv);

    List<String> stat = stats.get(name);

    LinearLayout statsLayout = new LinearLayout(activity);
    statsLayout.setLayoutParams(params);
    statsLayout.setOrientation(LinearLayout.VERTICAL);

    for (int idx = 0; idx < stat.size(); idx++) {
      LinearLayout row = new LinearLayout(activity);

      TextView gameTypeTv = new TextView(activity);
      params = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0f);
      gameTypeTv.setLayoutParams(params);
      gameTypeTv.setText(GameType.getDisplayNameForName(activity.getResources(), stat.get(idx++)));
      gameTypeTv.setTextSize(Constants.STATS_LIST_TEXT_SIZE);
      gameTypeTv.setGravity(Gravity.RIGHT);
      row.addView(gameTypeTv);

      TextView winsTv = new TextView(activity);
      winsTv.setLayoutParams(params);
      winsTv.setText(stat.get(idx++));
      winsTv.setTextColor(Constants.COLORS.get(Player.PLAYER));
      winsTv.setTextSize(Constants.STATS_LIST_TEXT_SIZE);
      winsTv.setGravity(Gravity.RIGHT);
      row.addView(winsTv);

      TextView lossesTv = new TextView(activity);
      lossesTv.setLayoutParams(params);
      lossesTv.setText(stat.get(idx++));
      lossesTv.setTextColor(Constants.COLORS.get(Player.COMPUTER));
      lossesTv.setTextSize(Constants.STATS_LIST_TEXT_SIZE);
      lossesTv.setGravity(Gravity.RIGHT);
      row.addView(lossesTv);

      TextView tiesTv = new TextView(activity);
      params.rightMargin = 35;
      tiesTv.setLayoutParams(params);
      tiesTv.setText(stat.get(idx));
      tiesTv.setTextSize(Constants.STATS_LIST_TEXT_SIZE);
      tiesTv.setGravity(Gravity.RIGHT);
      row.addView(tiesTv);

      statsLayout.addView(row);
    }

    layout.addView(statsLayout);

    return layout;
  }

}
