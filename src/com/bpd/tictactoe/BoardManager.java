package com.bpd.tictactoe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public final class BoardManager implements Serializable {

  private static final long serialVersionUID = 1L;

  enum Player {
    PLAYER("X"), COMPUTER("O");

    private final String marker;

    Player(String marker) {
      this.marker = marker;
    }

    public String getMarker() {
      return marker;
    }
  }

  private final GameType gameType;

  private final Player[] board;
  private Player currentPlayer;

  public BoardManager(GameType gameType) {
    if (gameType == null) {
      throw new IllegalArgumentException("ganeType is null");
    }
    this.gameType = gameType;
    board = gameType.getNewBoard();
  }

  public BoardManager(BoardManager boardManager) {
    this(boardManager.gameType);
  }

  GameType getGameType() {
    return gameType;
  }

  public Player[] getBoard() {
    Player[] copy = new Player[board.length];
    System.arraycopy(board, 0, copy, 0, board.length);
    return copy;
  }

  int getBoardSize() {
    return gameType.getSize();
  }

  List<Integer> getColumns() {
    List<Integer> list = new ArrayList<Integer>();
    for (int column : gameType.getColumns()) {
      list.add(column);
    }
    return list;
  }

  List<Integer> getRows() {
    List<Integer> list = new ArrayList<Integer>();
    for (int row : gameType.getRows()) {
      list.add(row);
    }
    return list;
  }

  List<Integer> getLeftToRightDiagonals() {
    List<Integer> list = new ArrayList<Integer>();
    for (int diagonal : gameType.getLeftToRightDiagonal()) {
      list.add(diagonal);
    }
    return list;
  }

  List<Integer> getRightToLeftDiagonals() {
    List<Integer> list = new ArrayList<Integer>();
    for (int diagonal : gameType.getRightToLeftDiagonal()) {
      list.add(diagonal);
    }
    return list;
  }

  public int getCornerPosition(int position) {
    int[] corners = gameType.getCorners();
    if (position < 0 || position >= corners.length) {
      throw new IllegalArgumentException("invalid position: " + position);
    }
    return corners[position];
  }

  public void setCurrentPlayer(Player player) {
    currentPlayer = player;
  }

  public void update(int position) {
    if (position < 0 || position >= board.length) {
      throw new IllegalArgumentException("invalid position: " + position);
    }
    board[position] = currentPlayer;
    currentPlayer = currentPlayer == Player.COMPUTER ? Player.PLAYER : Player.COMPUTER;
  }

  // Only call this method in unit tests.
  public void set(int position, Player player) {
    board[position] = player;
  }

  public Player getCurrentPlayer() {
    return currentPlayer;
  }

  public Player getPlayerAt(int position) {
    if (position < 0 || position >= board.length) {
      throw new IllegalArgumentException("invalid position: " + position);
    }
    return board[position];
  }

  public boolean isTie() {
    for (int i = 0; i < board.length; i++) {
      if (board[i] == null) {
        return false;
      }
    }
    return true;
  }

  public Player getWinner() {
    Player player = null;
    if ((player = checkRows()) != null) {
      return player;
    }
    if ((player = checkColumns()) != null) {
      return player;
    }
    if ((player = checkLeftToRightDiagonal(null)) != null) {
      return player;
    }
    if ((player = checkRightToLeftDiagonal(null)) != null) {
      return player;
    }
    return player;
  }

  private Player checkRows() {
    int[] rows = gameType.getRows();
    for (int pos : rows) {
      List<Player> row = new ArrayList<Player>();
      for (int i = 0; i < gameType.getSize(); i++) {
        row.add(board[pos + i]);
      }
      if (Collections.frequency(row, Player.PLAYER) == gameType.getSize()) {
        return Player.PLAYER;
      }
      if (Collections.frequency(row, Player.COMPUTER) == gameType.getSize()) {
        return Player.COMPUTER;
      }
    }
    return null;
  }

  private Player checkColumns() {
    int[] columns = gameType.getColumns();
    for (int pos : columns) {
      List<Player> column = new ArrayList<Player>();
      for (int i = 0; i < gameType.getSize(); i++) {
        column.add(board[pos + (i * gameType.getSize())]);
      }
      if (Collections.frequency(column, Player.PLAYER) == gameType.getSize()) {
        return Player.PLAYER;
      }
      if (Collections.frequency(column, Player.COMPUTER) == gameType.getSize()) {
        return Player.COMPUTER;
      }
    }
    return null;
  }

  private boolean checkDiagonal(int[] diagonal) {
    if (diagonal == null || diagonal.length != gameType.getSize()) {
      throw new IllegalArgumentException("diagonal is null or does not equal size of board");
    }
    List<Player> diag = new ArrayList<Player>();
    for (int pos : diagonal) {
      diag.add(board[pos]);
    }
    if (Collections.frequency(diag, Player.PLAYER) == gameType.getSize()) {
      return true;
    }
    if (Collections.frequency(diag, Player.COMPUTER) == gameType.getSize()) {
      return true;
    }
    return false;
  }

  Player checkLeftToRightDiagonal(Integer position) {
    if (checkDiagonal(gameType.getLeftToRightDiagonal())) {
      // It doesn't matter which one to return; all three are the same.
      return board[Constants.TOP_LEFT];
    }
    if (position == null) {
      return null;
    }
    if (gameType.isInLeftToRightDiagonal(position)) {
      return board[position];
    }
    else {
      throw new IllegalArgumentException("position is not in L-R diagonal: " + position);
    }
  }

  Player checkRightToLeftDiagonal(Integer position) {
    if (checkDiagonal(gameType.getRightToLeftDiagonal())) {
      // It doesn't matter which one to return; all three are the same.
      return board[gameType.getSize() - 1];
    }
    if (position == null) {
      return null;
    }
    if (gameType.isInRightToLeftDiagonal(position)) {
      return board[position];
    }
    else {
      throw new IllegalArgumentException("position is not in R-L diagonal: " + position);
    }
  }

  static Player getFirstPlayer() {
    Random random = new Random();
    return (1 + random.nextInt(2)) == 1 ? Player.PLAYER : Player.COMPUTER;
  }

  @Override
  public String toString() {
    String s = "";
    for (Player player : board) {
      s += player + " ";
    }
    return s;
  }

}
