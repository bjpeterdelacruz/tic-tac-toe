package com.bpd.tictactoe;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

  private SQLiteDatabase database;
  private final Context context;
  private final String databasePath;

  /**
   * Creates a new DatabaseHelper instance.
   * 
   * @param context The context.
   */
  public DatabaseHelper(Context context) {
    super(context, DatabaseConstants.DATABASE_NAME, null, 1);
    this.context = context;
    databasePath = context.getDatabasePath(DatabaseConstants.DATABASE_NAME).getPath();
  }

  /**
   * Creates an empty database on the system and overwrites it with our database.
   */
  void createDatabase() throws IOException {
    if (checkDatabase()) {
      return;
    }

    // By calling this method, an empty database will be created in the default system path
    // of our application so we can overwrite that database with our database.
    SQLiteDatabase db = getReadableDatabase();
    copyDatabase();
    db.close();
  }

  void upgradeDatabase() throws IOException {
    try {
      String column = DatabaseConstants.GAME_TYPE_COLUMN_NAME;
      String table = DatabaseConstants.RECORDS_TABLE;
      String query = String.format("SELECT %s FROM %s LIMIT 1", column, table);
      database.rawQuery(query, null);
      return;
    }
    catch (Exception e) {
      // Column doesn't exist. User had old version of app installed, so upgrade database.
    }

    // Save all old data
    String query = "SELECT * FROM " + DatabaseConstants.USERS_TABLE;
    Cursor c = database.rawQuery(query, null);
    List<List<Object>> values1 = new ArrayList<List<Object>>();
    if (c.moveToFirst()) {
      while (!c.isAfterLast()) {
        List<Object> record = new ArrayList<Object>();
        record.add(c.getInt(0));
        record.add(c.getString(1));
        values1.add(record);
        c.moveToNext();
      }
    }
    c.close();

    query = "SELECT * FROM " + DatabaseConstants.RECORDS_TABLE;
    c = database.rawQuery(query, null);
    List<List<Object>> values2 = new ArrayList<List<Object>>();
    if (c.moveToFirst()) {
      while (!c.isAfterLast()) {
        List<Object> record = new ArrayList<Object>();
        record.add(c.getInt(0));
        record.add(c.getInt(1));
        record.add(c.getInt(2));
        record.add(c.getInt(3));
        values2.add(record);
        c.moveToNext();
      }
    }
    c.close();

    // Copy empty database with new schema
    copyDatabase();

    // Restore all old data
    for (List<Object> record : values1) {
      ContentValues cv = new ContentValues();
      cv.put(DatabaseConstants.ID_COLUMN_NAME, (Integer) record.get(0));
      cv.put(DatabaseConstants.USERNAME_COLUMN_NAME, record.get(1).toString());
      database.insert(DatabaseConstants.USERS_TABLE, null, cv);
    }
    for (List<Object> record : values2) {
      ContentValues cv = new ContentValues();
      cv.put(DatabaseConstants.USER_ID_COLUMN_NAME, (Integer) record.get(0));
      cv.put(DatabaseConstants.GAME_TYPE_COLUMN_NAME, GameType.CLASSIC.name());
      cv.put(DatabaseConstants.WINS_COLUMN_NAME, (Integer) record.get(1));
      cv.put(DatabaseConstants.LOSSES_COLUMN_NAME, (Integer) record.get(2));
      cv.put(DatabaseConstants.TIES_COLUMN_NAME, (Integer) record.get(3));
      database.insert(DatabaseConstants.RECORDS_TABLE, null, cv);
    }
  }

  /**
   * Checks if the database already exists to avoid re-copying the file each time the application opens.
   * 
   * @return True if the database exists, false otherwise.
   */
  private boolean checkDatabase() {
    try {
      SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READWRITE).close();
      return true;
    }
    catch (SQLiteException e) {
      // Database doesn't exist yet.
      return false;
    }
  }

  /**
   * Copies our database from the assets folder to the newly created empty database in the system folder from where it
   * can be accessed and handled.
   */
  void copyDatabase() throws IOException {
    InputStream is = context.getAssets().open(DatabaseConstants.DATABASE_NAME);
    OutputStream os = new FileOutputStream(databasePath);

    byte[] buffer = new byte[1024];
    int length;
    while ((length = is.read(buffer)) > 0) {
      os.write(buffer, 0, length);
    }

    // Close the streams.
    os.flush();
    os.close();
    is.close();
  }

  /**
   * Opens the database.
   * 
   * @throws SQLException If there were problems opening the database.
   */
  void openDatabase() throws SQLException {
    database = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READWRITE);
  }

  @Override
  public synchronized void close() {
    if (database != null) {
      database.close();
    }
    super.close();
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
  }

  /**
   * This method should be called in a separate thread.
   * 
   * @param data The data to insert into the database.
   */
  void insertUser(String username) {
    if (username == null) {
      throw new IllegalArgumentException("data is null");
    }
    ContentValues cv = new ContentValues();
    cv.put(DatabaseConstants.USERNAME_COLUMN_NAME, username);
    database.insert(DatabaseConstants.USERS_TABLE, null, cv);
  }

  @SuppressWarnings("resource")
  List<String> getStats(String username, GameType gameType) {
    if (username == null || username.length() == 0) {
      throw new IllegalArgumentException("username is null or empty");
    }
    Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_USER_ID, new String[] { username });
    if (!cursor.moveToFirst()) {
      cursor.close();
      if (gameType == null) {
        return new ArrayList<String>(Arrays.asList("-", "0", "0", "0"));
      }
      return new ArrayList<String>(Arrays.asList("0", "0", "0"));
    }
    String userId = String.valueOf(cursor.getInt(0));
    cursor.close();
    if (gameType == null) {
      cursor = database.rawQuery(DatabaseConstants.GET_ALL_STATS_FOR_USER, new String[] { userId });
    }
    else {
      cursor = database.rawQuery(DatabaseConstants.GET_STATS_FOR_USER, new String[] { userId, gameType.name() });
    }
    if (!cursor.moveToFirst()) {
      cursor.close();
      if (gameType == null) {
        return new ArrayList<String>(Arrays.asList("-", "0", "0", "0"));
      }
      String wins = "0";
      String losses = "0";
      String draws = "0";
      insertOrUpdateStats(username, gameType, wins, losses, draws);
      return new ArrayList<String>(Arrays.asList(wins, losses, draws));
    }
    List<String> stats = new ArrayList<String>();
    if (gameType == null) {
      while (!cursor.isAfterLast()) {
        stats.add(cursor.getString(0));
        stats.add(String.valueOf(cursor.getInt(1)));
        stats.add(String.valueOf(cursor.getInt(2)));
        stats.add(String.valueOf(cursor.getInt(3)));
        cursor.moveToNext();
      }
    }
    else {
      stats.add(String.valueOf(cursor.getInt(0)));
      stats.add(String.valueOf(cursor.getInt(1)));
      stats.add(String.valueOf(cursor.getInt(2)));
    }
    cursor.close();
    return stats;
  }

  Map<String, List<String>> getStatsForAllUsers() {
    Map<String, List<String>> stats = new HashMap<String, List<String>>();
    for (String user : getAllUsers()) {
      stats.put(user, getStats(user, null));
    }
    return stats;
  }

  void insertOrUpdateStats(String username, GameType gameType, String wins, String losses, String draws) {
    Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_USER_ID, new String[] { username });
    if (!cursor.moveToFirst()) {
      cursor.close();
      return;
    }
    String userId = String.valueOf(cursor.getInt(0));
    cursor.close();
    cursor = database.rawQuery(DatabaseConstants.GET_STATS_FOR_USER, new String[] { userId, gameType.name() });
    if (cursor.moveToFirst()) {
      cursor.close();
      ContentValues cv = new ContentValues();
      cv.put(DatabaseConstants.GAME_TYPE_COLUMN_NAME, gameType.name());
      cv.put(DatabaseConstants.WINS_COLUMN_NAME, Integer.parseInt(wins));
      cv.put(DatabaseConstants.LOSSES_COLUMN_NAME, Integer.parseInt(losses));
      cv.put(DatabaseConstants.TIES_COLUMN_NAME, Integer.parseInt(draws));
      String whereClause = DatabaseConstants.USER_ID_COLUMN_NAME + " = " + userId + " AND ";
      whereClause = whereClause + DatabaseConstants.GAME_TYPE_COLUMN_NAME + " = '" + gameType.name() + "'";
      database.update(DatabaseConstants.RECORDS_TABLE, cv, whereClause, null);
    }
    else {
      cursor.close();
      ContentValues cv = new ContentValues();
      cv.put(DatabaseConstants.USER_ID_COLUMN_NAME, Integer.parseInt(userId));
      cv.put(DatabaseConstants.GAME_TYPE_COLUMN_NAME, gameType.name());
      cv.put(DatabaseConstants.WINS_COLUMN_NAME, Integer.parseInt(wins));
      cv.put(DatabaseConstants.LOSSES_COLUMN_NAME, Integer.parseInt(losses));
      cv.put(DatabaseConstants.TIES_COLUMN_NAME, Integer.parseInt(draws));
      database.insert(DatabaseConstants.RECORDS_TABLE, null, cv);
    }
  }

  void removeUser(String username) {
    Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_USER_ID, new String[] { username });
    if (!cursor.moveToFirst()) {
      cursor.close();
      return;
    }
    String userId = String.valueOf(cursor.getInt(0));
    cursor.close();
    String whereClause = DatabaseConstants.USERNAME_COLUMN_NAME + " = '" + username + "'";
    database.delete(DatabaseConstants.USERS_TABLE, whereClause, null);
    whereClause = DatabaseConstants.USER_ID_COLUMN_NAME + " = " + userId;
    database.delete(DatabaseConstants.RECORDS_TABLE, whereClause, null);
  }

  List<String> getAllUsers() {
    List<String> users = new ArrayList<String>();
    Cursor cursor = database.rawQuery(DatabaseConstants.SELECT_ALL_USERS, null);
    if (!cursor.moveToFirst()) {
      cursor.close();
      return users;
    }
    while (!cursor.isAfterLast()) {
      users.add(cursor.getString(0));
      cursor.moveToNext();
    }
    cursor.close();
    return users;
  }

}
