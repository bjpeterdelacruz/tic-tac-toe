package com.bpd.tictactoe;

public final class DatabaseConstants {

  private DatabaseConstants() {
  }

  static final String DATABASE_NAME = "data.db";

  static final String USERS_TABLE = "users";

  static final String ID_COLUMN_NAME = "_id";
  static final String USERNAME_COLUMN_NAME = "username";

  static final String RECORDS_TABLE = "records";

  static final String USER_ID_COLUMN_NAME = "userId";
  static final String GAME_TYPE_COLUMN_NAME = "gameType";
  static final String WINS_COLUMN_NAME = "wins";
  static final String LOSSES_COLUMN_NAME = "losses";
  static final String TIES_COLUMN_NAME = "ties";

  static final String SELECT_ALL_USERS = "SELECT username FROM " + USERS_TABLE;

  static final String SELECT_USER_ID = "SELECT " + ID_COLUMN_NAME + " FROM " + USERS_TABLE + " WHERE "
      + USERNAME_COLUMN_NAME + " = ?";

  static final String GET_ALL_STATS_FOR_USER = "SELECT " + GAME_TYPE_COLUMN_NAME + ", " + WINS_COLUMN_NAME + ", "
      + LOSSES_COLUMN_NAME + ", " + TIES_COLUMN_NAME + " FROM " + RECORDS_TABLE + " WHERE " + USER_ID_COLUMN_NAME
      + " = ?";

  static final String GET_STATS_FOR_USER = "SELECT " + WINS_COLUMN_NAME + ", " + LOSSES_COLUMN_NAME + ", "
      + TIES_COLUMN_NAME + " FROM " + RECORDS_TABLE + " WHERE " + USER_ID_COLUMN_NAME + " = ? AND "
      + GAME_TYPE_COLUMN_NAME + " = ?";

}
