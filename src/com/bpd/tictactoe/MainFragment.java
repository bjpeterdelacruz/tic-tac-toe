package com.bpd.tictactoe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.bpd.tictactoe.BoardManager.Player;

public class MainFragment extends Fragment {

  private final List<Button> buttons = new ArrayList<Button>();

  private MainActivity activity;
  private BoardManager boardManager;
  private ComputerPlayer computerPlayer;

  private Resources res;

  private TextView winsTv, lossesTv, tiesTv;

  private String currentUser;

  private boolean isUsersTurn, isNewGame;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    activity = (MainActivity) getActivity();
    res = activity.getResources();

    int orientation = res.getConfiguration().orientation;

    TableLayout table = new TableLayout(activity);
    table.setStretchAllColumns(true);
    table.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

    Bundle args = getArguments();
    currentUser = args.getString(Constants.CURRENT_USER_KEY);

    winsTv = new TextView(activity);
    winsTv.setTextColor(Constants.COLORS.get(Player.PLAYER));
    winsTv.setTextSize(Constants.STATS_TEXT_SIZE);
    winsTv.setGravity(Gravity.CENTER);

    lossesTv = new TextView(activity);
    lossesTv.setTextColor(Constants.COLORS.get(Player.COMPUTER));
    lossesTv.setTextSize(Constants.STATS_TEXT_SIZE);
    lossesTv.setGravity(Gravity.CENTER);

    tiesTv = new TextView(activity);
    tiesTv.setTextSize(Constants.STATS_TEXT_SIZE);
    tiesTv.setGravity(Gravity.CENTER);

    if (savedInstanceState == null) {
      retrieveStats();
    }
    else {
      String wins = savedInstanceState.getString(Constants.WINS_KEY);
      if (wins == null) {
        retrieveStats();
      }
      else {
        winsTv.setText(wins);
        lossesTv.setText(savedInstanceState.getString(Constants.LOSSES_KEY));
        tiesTv.setText(savedInstanceState.getString(Constants.TIES_KEY));
      }
    }

    buttons.clear();

    GradientDrawable gd = new GradientDrawable();
    gd.setColor(Color.TRANSPARENT);
    gd.setCornerRadius(5);
    gd.setStroke(1, 0xFF000000);

    for (int i = 0, idx = 0; i < boardManager.getBoardSize(); i++) {
      TableRow row = createRow();

      for (int j = 0; j < boardManager.getBoardSize(); j++, idx++) {
        Button button = new Button(activity);
        button.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
            TableRow.LayoutParams.MATCH_PARENT));
        button.setId(idx);
        button.setOnClickListener(new ButtonListener(button));
        if (activity.isPhone() && res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
          button.setTextSize(Constants.X_O_TEXT_SIZE_PHONE);
        }
        else {
          button.setTextSize(Constants.X_O_TEXT_SIZE);
        }
        button.setBackgroundDrawable(gd);
        buttons.add(button);
        row.addView(button);
      }

      table.addView(row);
    }

    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
    params.weight = 0.25f;

    int id = res.getIdentifier("new_game", "drawable", getActivity().getPackageName());
    Bitmap bitmap = BitmapFactory.decodeStream(res.openRawResource(id));

    final ImageButton newGameButton = new ImageButton(activity);
    newGameButton.setLayoutParams(params);
    newGameButton.setBackgroundColor(Color.TRANSPARENT);
    newGameButton.setImageBitmap(bitmap);
    newGameButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        for (Button button : buttons) {
          if (button.getText().toString().length() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            final AlertDialog dialog =
                builder.setCancelable(false).setTitle(res.getString(R.string.game_in_progress))
                    .setMessage(res.getString(R.string.confirm_new_game))
                    .setPositiveButton(res.getString(R.string.yes), new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                        startOver();
                      }
                    }).setNegativeButton(res.getString(R.string.no), new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                      }
                    }).create();
            dialog.show();
            return;
          }
        }
        startOver();
      }
    });

    id = res.getIdentifier("scores", "drawable", getActivity().getPackageName());
    bitmap = BitmapFactory.decodeStream(res.openRawResource(id));

    final ImageButton statsButton = new ImageButton(activity);
    statsButton.setLayoutParams(params);
    statsButton.setBackgroundColor(Color.TRANSPARENT);
    statsButton.setImageBitmap(bitmap);
    statsButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        updateStats();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        String tag = Constants.STATS_FRAGMENT;
        fragmentManager.beginTransaction().replace(R.id.fragment_container, new StatsFragment(), tag)
            .addToBackStack(null).commit();
      }

    });

    id = res.getIdentifier("delete", "drawable", getActivity().getPackageName());
    bitmap = BitmapFactory.decodeStream(res.openRawResource(id));

    final ImageButton deleteButton = new ImageButton(activity);
    deleteButton.setLayoutParams(params);
    deleteButton.setBackgroundColor(Color.TRANSPARENT);
    deleteButton.setImageBitmap(bitmap);
    deleteButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final AlertDialog dialog =
            builder.setCancelable(false).setTitle(res.getString(R.string.reset_stats))
                .setMessage(res.getString(R.string.confirm_reset))
                .setPositiveButton(res.getString(R.string.yes), new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    winsTv.setText("0");
                    lossesTv.setText("0");
                    tiesTv.setText("0");
                    updateStats();
                  }
                }).setNegativeButton(res.getString(R.string.no), new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                  }
                }).create();
        dialog.show();
      }

    });

    id = res.getIdentifier("exit", "drawable", getActivity().getPackageName());
    bitmap = BitmapFactory.decodeStream(res.openRawResource(id));

    final ImageButton quitButton = new ImageButton(activity);
    quitButton.setLayoutParams(params);
    quitButton.setBackgroundColor(Color.TRANSPARENT);
    quitButton.setImageBitmap(bitmap);
    quitButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final AlertDialog dialog =
            builder.setCancelable(false).setTitle(res.getString(R.string.game_in_progress))
                .setMessage(res.getString(R.string.confirm_quit))
                .setPositiveButton(res.getString(R.string.yes), new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    updateStats();
                    activity.finish();
                  }
                }).setNegativeButton(res.getString(R.string.no), new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                  }
                }).create();
        dialog.show();
      }
    });

    if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
      LinearLayout landscapeLayout = new LinearLayout(activity);

      LinearLayout leftPanel1 = new LinearLayout(activity);
      leftPanel1.setOrientation(LinearLayout.VERTICAL);
      params = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 0.5f);
      leftPanel1.setLayoutParams(params);

      params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
      newGameButton.setLayoutParams(params);
      leftPanel1.addView(newGameButton);
      statsButton.setLayoutParams(params);
      leftPanel1.addView(statsButton);
      deleteButton.setLayoutParams(params);
      leftPanel1.addView(deleteButton);
      quitButton.setLayoutParams(params);
      leftPanel1.addView(quitButton);

      LinearLayout leftPanel2 = new LinearLayout(activity);
      leftPanel2.setOrientation(LinearLayout.VERTICAL);
      params = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 0.5f);
      leftPanel2.setLayoutParams(params);

      params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
      winsTv.setLayoutParams(params);
      leftPanel2.addView(winsTv);
      lossesTv.setLayoutParams(params);
      leftPanel2.addView(lossesTv);
      tiesTv.setLayoutParams(params);
      leftPanel2.addView(tiesTv);

      LinearLayout rightPanel = new LinearLayout(activity);
      rightPanel.setLayoutParams(new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 2.0f));
      rightPanel.addView(table);
      landscapeLayout.addView(leftPanel1);
      landscapeLayout.addView(leftPanel2);
      landscapeLayout.addView(rightPanel);

      return landscapeLayout;
    }
    else {
      LinearLayout statsRow = new LinearLayout(activity);
      statsRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
          LinearLayout.LayoutParams.MATCH_PARENT));

      params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
      winsTv.setLayoutParams(params);
      lossesTv.setLayoutParams(params);
      tiesTv.setLayoutParams(params);

      statsRow.addView(winsTv);
      statsRow.addView(lossesTv);
      statsRow.addView(tiesTv);

      LinearLayout timerRow = new LinearLayout(activity);
      timerRow.addView(statsRow);
      table.addView(timerRow);

      LinearLayout buttonRow = new LinearLayout(activity);
      buttonRow.addView(newGameButton);
      buttonRow.addView(statsButton);
      buttonRow.addView(deleteButton);
      buttonRow.addView(quitButton);

      TableRow row = new TableRow(activity);
      row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

      row.addView(buttonRow);
      ((TableRow.LayoutParams) buttonRow.getLayoutParams()).span = boardManager.getBoardSize();
      table.addView(row);

      return table;
    }
  }

  private void retrieveStats() {
    new AsyncTask<Void, Void, List<String>>() {

      @Override
      protected List<String> doInBackground(Void... params) {
        DatabaseHelper helper = null;
        try {
          helper = new DatabaseHelper(activity);
          helper.openDatabase();
          return helper.getStats(currentUser, boardManager.getGameType());
        }
        catch (SQLException sqle) {
          Log.e(MainFragment.class.getCanonicalName(), sqle.getMessage(), sqle);
          return Arrays.asList("0", "0", "0");
        }
        finally {
          if (helper != null) {
            helper.close();
          }
        }
      }

      @Override
      protected void onPostExecute(List<String> results) {
        winsTv.setText(results.get(0));
        lossesTv.setText(results.get(1));
        tiesTv.setText(results.get(2));
      }

    }.execute();
  }

  void updateStats() {
    final String wins = winsTv.getText().toString();
    final String losses = lossesTv.getText().toString();
    final String ties = tiesTv.getText().toString();
    new AsyncTask<Void, Void, Void>() {

      @Override
      protected Void doInBackground(Void... params) {
        DatabaseHelper helper = null;
        try {
          helper = new DatabaseHelper(activity);
          helper.openDatabase();
          helper.insertOrUpdateStats(currentUser, boardManager.getGameType(), wins, losses, ties);
        }
        catch (SQLException sqle) {
          Log.e(MainFragment.class.getCanonicalName(), sqle.getMessage(), sqle);
        }
        finally {
          if (helper != null) {
            helper.close();
          }
        }
        return null;
      }

    }.execute();
  }

  private void startOver() {
    boardManager = new BoardManager(boardManager);
    boardManager.setCurrentPlayer(BoardManager.getFirstPlayer());
    computerPlayer = new ComputerPlayer(boardManager);
    isNewGame = true;
    for (Button button : buttons) {
      button.setText("");
      button.setEnabled(true);
    }
    isUsersTurn = boardManager.getCurrentPlayer() == Player.PLAYER;
    if (!isUsersTurn) {
      new AsyncTask<Void, Void, Void>() {

        @Override
        protected Void doInBackground(Void... args) {
          try {
            Thread.sleep(Constants.COMPUTER_SLEEP_MILLIS);
          }
          catch (Exception e) {
          }
          return null;
        }

        @Override
        protected void onPostExecute(Void results) {
          updateComputerMove(computerPlayer.move());
        }

      }.execute();
    }
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (savedInstanceState == null) {
      Bundle args = getArguments();
      boardManager = (BoardManager) args.getSerializable(Constants.BOARD_MANAGER_KEY);
      boardManager.setCurrentPlayer((Player) args.getSerializable(Constants.CURRENT_PLAYER_KEY));
      computerPlayer = (ComputerPlayer) args.getSerializable(Constants.COMPUTER_PLAYER_KEY);
      isNewGame = true;
    }
    else {
      boardManager = (BoardManager) savedInstanceState.getSerializable(Constants.BOARD_MANAGER_KEY);
      boardManager.setCurrentPlayer((Player) savedInstanceState.getSerializable(Constants.CURRENT_PLAYER_KEY));
      computerPlayer = (ComputerPlayer) savedInstanceState.getSerializable(Constants.COMPUTER_PLAYER_KEY);
      isNewGame = savedInstanceState.getBoolean(Constants.IS_NEW_GAME_KEY);
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    configure();
  }

  @Override
  public void onSaveInstanceState(Bundle savedInstanceState) {
    super.onSaveInstanceState(savedInstanceState);
    savedInstanceState.putSerializable(Constants.BOARD_MANAGER_KEY, boardManager);
    savedInstanceState.putSerializable(Constants.CURRENT_PLAYER_KEY, boardManager.getCurrentPlayer());
    savedInstanceState.putSerializable(Constants.COMPUTER_PLAYER_KEY, computerPlayer);
    savedInstanceState.putBoolean(Constants.IS_NEW_GAME_KEY, isNewGame);
    if (winsTv != null) {
      savedInstanceState.putString(Constants.WINS_KEY, winsTv.getText().toString());
    }
    if (lossesTv != null) {
      savedInstanceState.putString(Constants.LOSSES_KEY, lossesTv.getText().toString());
    }
    if (tiesTv != null) {
      savedInstanceState.putString(Constants.TIES_KEY, tiesTv.getText().toString());
    }
  }

  private TableRow createRow() {
    TableRow row = new TableRow(activity);
    row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
        TableLayout.LayoutParams.MATCH_PARENT, 1.0f));
    return row;
  }

  private void configure() {
    for (Button button : buttons) {
      button.setOnClickListener(new ButtonListener(button));
      if (isNewGame) {
        button.setText("");
        button.setEnabled(true);
      }
      else {
        Player player = boardManager.getPlayerAt(button.getId());
        if (player == null) {
          button.setText("");
          button.setEnabled(true);
        }
        else {
          button.setText(player.getMarker());
          button.setTextColor(Constants.COLORS.get(player));
          button.setEnabled(false);
        }
      }
    }

    isUsersTurn = boardManager.getCurrentPlayer() == Player.PLAYER;
    if (isNewGame && !isUsersTurn) {
      updateComputerMove(computerPlayer.move());
    }
  }

  private void updateComputerMove(int position) {
    if (position == Constants.NO_MOVE) {
      return;
    }
    Button button = buttons.get(position);
    button.setText(Player.COMPUTER.getMarker());
    button.setTextColor(Constants.COLORS.get(Player.COMPUTER));
    button.setEnabled(false);
    isUsersTurn = true;
    isNewGame = false;
  }

  private class ButtonListener implements OnClickListener {

    private final Button button;

    private ButtonListener(Button button) {
      this.button = button;
    }

    @Override
    public void onClick(View v) {
      if (!isUsersTurn) {
        return;
      }
      isUsersTurn = false;
      isNewGame = false;
      button.setText(Player.PLAYER.getMarker());
      button.setTextColor(Constants.COLORS.get(Player.PLAYER));
      button.setEnabled(false);
      boardManager.update(button.getId());
      if (!isGameOver()) {
        new AsyncTask<Void, Void, Void>() {

          @Override
          protected Void doInBackground(Void... args) {
            try {
              Thread.sleep(Constants.COMPUTER_SLEEP_MILLIS);
            }
            catch (Exception e) {
            }
            return null;
          }

          @Override
          protected void onPostExecute(Void result) {
            // Now it's the computer's turn
            updateComputerMove(computerPlayer.move());
            isGameOver();
          }

        }.execute();
      }
    }
  }

  private boolean isGameOver() {
    Player player = boardManager.getWinner();
    if (player == null) {
      if (boardManager.isTie()) {
        int ties = Integer.parseInt(tiesTv.getText().toString());
        tiesTv.setText(String.valueOf(++ties));
        displayEndGameMessage(res.getString(R.string.tie));
        return true;
      }
      return false;
    }
    for (Button button : buttons) {
      button.setEnabled(false);
    }
    String title;
    switch (player) {
    case PLAYER:
      int wins = Integer.parseInt(winsTv.getText().toString());
      winsTv.setText(String.valueOf(++wins));
      title = res.getString(R.string.you_won);
      break;
    case COMPUTER:
      int losses = Integer.parseInt(lossesTv.getText().toString());
      lossesTv.setText(String.valueOf(++losses));
      title = res.getString(R.string.game_over);
      break;
    default:
      throw new IllegalArgumentException("Invalid player type: " + player);
    }
    displayEndGameMessage(title);
    return true;
  }

  private void displayEndGameMessage(String title) {
    if (title == null) {
      throw new IllegalArgumentException("title is null");
    }
    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
    builder.setCancelable(false).setTitle(title).setMessage(res.getString(R.string.try_again))
        .setPositiveButton(res.getString(R.string.yes), new YesButtonListener())
        .setNegativeButton(res.getString(R.string.no), new NoButtonListener()).create().show();
  }

  private class YesButtonListener implements DialogInterface.OnClickListener {
    @Override
    public void onClick(DialogInterface dialog, int which) {
      startOver();
    }
  }

  private class NoButtonListener implements DialogInterface.OnClickListener {
    @Override
    public void onClick(DialogInterface dialog, int which) {
      updateStats();
      activity.finish();
    }
  }

}
