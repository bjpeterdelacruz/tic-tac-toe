package com.bpd.tictactoe;

import java.util.ArrayList;
import java.util.List;
import android.content.res.Resources;
import com.bpd.tictactoe.BoardManager.Player;

public enum GameType {

  CLASSIC(3), FOUR_IN_A_ROW(4), GOMOKU(5);

  private int size;

  GameType(int size) {
    this.size = size;
  }

  int getSize() {
    return size;
  }

  static GameType getGameTypeForSize(int size) {
    for (GameType type : GameType.values()) {
      if (type.size == size) {
        return type;
      }
    }
    throw new IllegalArgumentException("unsupported size: " + size);
  }

  static String getDisplayNameForName(Resources res, String name) {
    if (res == null) {
      throw new IllegalArgumentException("res is null");
    }
    if (name == null || name.length() == 0) {
      throw new IllegalArgumentException("name is null or empty");
    }
    if (CLASSIC.name().equals(name)) {
      return res.getString(R.string.classic_short);
    }
    else if (FOUR_IN_A_ROW.name().equals(name)) {
      return res.getString(R.string.four_short);
    }
    else if (GOMOKU.name().equals(name)) {
      return res.getString(R.string.gomoku_short);
    }
    else {
      throw new IllegalArgumentException("invalid name: " + name);
    }
  }

  Player[] getNewBoard() {
    return new Player[size * size];
  }

  int[] getCorners() {
    return new int[] { 0, size - 1, (size - 1) * size, size * size - 1 };
  }

  int[] getRows() {
    int[] rows = new int[size];
    for (int i = 0; i < size; i++) {
      rows[i] = i * size;
    }
    return rows;
  }

  int[] getColumns() {
    int[] columns = new int[size];
    for (int i = 0; i < size; i++) {
      columns[i] = i;
    }
    return columns;
  }

  int[] getLeftToRightDiagonal() {
    int[] diagonal = new int[size];
    for (int i = 0; i < size; i++) {
      diagonal[i] = i * (size + 1);
    }
    return diagonal;
  }

  boolean isInLeftToRightDiagonal(int position) {
    List<Integer> list = new ArrayList<Integer>();
    for (int diagonal : getLeftToRightDiagonal()) {
      list.add(diagonal);
    }
    return list.contains(position);
  }

  int[] getRightToLeftDiagonal() {
    int[] diagonal = new int[size];
    for (int i = 1; i <= size; i++) {
      diagonal[i - 1] = i * (size - 1);
    }
    return diagonal;
  }

  boolean isInRightToLeftDiagonal(int position) {
    List<Integer> list = new ArrayList<Integer>();
    for (int diagonal : getRightToLeftDiagonal()) {
      list.add(diagonal);
    }
    return list.contains(position);
  }

}
