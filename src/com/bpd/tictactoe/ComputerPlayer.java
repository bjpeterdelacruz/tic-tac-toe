package com.bpd.tictactoe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import com.bpd.tictactoe.BoardManager.Player;

public final class ComputerPlayer implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final boolean DEBUG = false;

  private final BoardManager boardManager;
  private final Random random = new Random();

  public ComputerPlayer(BoardManager boardManager) {
    if (boardManager == null) {
      throw new IllegalArgumentException("boardManager is null");
    }
    this.boardManager = boardManager;
  }

  private int moveToRandomEmptyRowPosition() {
    Player[] board = boardManager.getBoard();
    List<Integer> rows = new ArrayList<Integer>(boardManager.getRows());
    Collections.shuffle(rows);
    for (int pos : rows) {
      for (int idx = 0; idx < boardManager.getBoardSize(); idx++) {
        if (board[pos + idx] == null) {
          return pos + idx;
        }
      }
    }
    return Constants.NO_MOVE;
  }

  private int moveToRandomEmptyColumnPosition() {
    Player[] board = boardManager.getBoard();
    List<Integer> columns = new ArrayList<Integer>(boardManager.getColumns());
    Collections.shuffle(columns);
    for (int pos : columns) {
      for (int idx = 0; idx < columns.size(); idx += columns.size()) {
        if (board[pos + idx] == null) {
          return pos + idx;
        }
      }
    }
    return Constants.NO_MOVE;
  }

  private int moveToDiagonalPosition() {
    for (int position : boardManager.getLeftToRightDiagonals()) {
      if (boardManager.checkLeftToRightDiagonal(position) == null) {
        return position;
      }
    }
    for (int position : boardManager.getRightToLeftDiagonals()) {
      if (boardManager.checkRightToLeftDiagonal(position) == null) {
        return position;
      }
    }
    return Constants.NO_MOVE;
  }

  public int move() {
    // Always try to win or block the player.
    int position;
    if ((position = winOrBlock()) != Constants.NO_MOVE) {
      return position;
    }

    // Sometimes pick center.
    position = Constants.CENTER;
    if ((generateRandomNumber(1, 24) <= 6 || DEBUG) && boardManager.getPlayerAt(position) == null) {
      return updateBoard(position);
    }

    // Sometimes pick corner.
    position = boardManager.getCornerPosition(generateRandomNumber(0, 3));
    if (boardManager.getPlayerAt(position) == null) {
      return updateBoard(position);
    }

    if ((position = moveToRandomEmptyRowPosition()) != Constants.NO_MOVE) {
      return updateBoard(position);
    }
    if ((position = moveToRandomEmptyColumnPosition()) != Constants.NO_MOVE) {
      return updateBoard(position);
    }
    if ((position = moveToDiagonalPosition()) != Constants.NO_MOVE) {
      return updateBoard(position);
    }

    // Pick any available space.
    for (position = Constants.TOP_LEFT; position <= Constants.BOTTOM_RIGHT; position++) {
      if (boardManager.getPlayerAt(position) == null) {
        return updateBoard(position);
      }
    }
    return Constants.NO_MOVE;
  }

  private int generateRandomNumber(int from, int to) {
    return from + random.nextInt(to - from + 1);
  }

  private int updateBoard(int position) {
    if (position == Constants.NO_MOVE) {
      return Constants.NO_MOVE;
    }
    boardManager.update(position);
    return position;
  }

  private Integer checkRow(int pos, Player player) {
    Player[] board = boardManager.getBoard();
    List<Player> moves = new ArrayList<Player>();
    int boardSize = boardManager.getBoardSize();
    Integer position = null;
    for (int idx = pos; idx < pos + boardSize; idx++) {
      if (board[idx] == null) {
        position = idx;
      }
      else if (board[idx] == player) {
        moves.add(player);
      }
    }
    if (moves.size() == boardSize - 1) {
      if (Collections.frequency(moves, player) == boardSize - 1) {
        return position;
      }
    }
    return null;
  }

  private int getRowWithOneMoveLeft() {
    Player[] players = new Player[] { Player.COMPUTER, Player.PLAYER };
    for (Player player : players) {
      for (int pos : boardManager.getRows()) {
        Integer position;
        if ((position = checkRow(pos, player)) != null) {
          return position;
        }
      }
    }
    return Constants.NO_MOVE;
  }

  private Integer checkColumn(int pos, Player player) {
    Player[] board = boardManager.getBoard();
    List<Player> moves = new ArrayList<Player>();
    int boardSize = boardManager.getBoardSize();
    Integer position = null;
    for (int idx = pos; idx < board.length; idx += boardSize) {
      if (board[idx] == null) {
        position = idx;
      }
      else if (board[idx] == player) {
        moves.add(player);
      }
    }
    if (moves.size() == boardSize - 1) {
      if (Collections.frequency(moves, player) == boardSize - 1) {
        return position;
      }
    }
    return null;
  }

  private int getColumnWithOneMoveLeft() {
    Player[] players = new Player[] { Player.COMPUTER, Player.PLAYER };
    for (Player player : players) {
      for (int pos : boardManager.getColumns()) {
        Integer position;
        if ((position = checkColumn(pos, player)) != null) {
          return position;
        }
      }
    }
    return Constants.NO_MOVE;
  }

  private Integer checkDiagonal(Player player, boolean isLeftToRightDiagonal) {
    Player[] board = boardManager.getBoard();
    List<Integer> diagonals;
    List<Player> moves = new ArrayList<Player>();
    Integer position = null;
    if (isLeftToRightDiagonal) {
      diagonals = boardManager.getLeftToRightDiagonals();
      int size = diagonals.size();
      for (int idx = 0; idx < size; idx++) {
        int pos = idx * (size + 1);
        if (board[pos] == null) {
          position = pos;
        }
        else if (board[pos] == player) {
          moves.add(board[pos]);
        }
      }
    }
    else {
      diagonals = boardManager.getRightToLeftDiagonals();
      int size = diagonals.size();
      for (int idx = 1; idx <= size; idx++) {
        int pos = idx * (size - 1);
        if (board[pos] == null) {
          position = pos;
        }
        else if (board[pos] == player) {
          moves.add(board[pos]);
        }
      }
    }
    if (moves.size() == diagonals.size() - 1) {
      if (Collections.frequency(moves, player) == diagonals.size() - 1) {
        return position;
      }
    }
    return null;
  }

  private int getDiagonalWithOneMoveLeft() {
    Player[] players = new Player[] { Player.COMPUTER, Player.PLAYER };
    for (Player player : players) {
      Integer position;
      if ((position = checkDiagonal(player, true)) != null) {
        return position;
      }
      if ((position = checkDiagonal(player, false)) != null) {
        return position;
      }
    }
    return Constants.NO_MOVE;
  }

  private int winOrBlock() {
    int position;
    if ((position = getRowWithOneMoveLeft()) != Constants.NO_MOVE) {
      return updateBoard(position);
    }
    if ((position = getColumnWithOneMoveLeft()) != Constants.NO_MOVE) {
      return updateBoard(position);
    }
    if ((position = getDiagonalWithOneMoveLeft()) != Constants.NO_MOVE) {
      return updateBoard(position);
    }
    return Constants.NO_MOVE;
  }

}
