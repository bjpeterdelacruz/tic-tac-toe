package com.bpd.tictactoe;

import java.util.EnumMap;
import java.util.Map;
import android.graphics.Color;
import com.bpd.tictactoe.BoardManager.Player;

public final class Constants {

  private Constants() {
  }

  static final String BOARD_MANAGER_KEY = "BoardManager";
  static final String COMPUTER_PLAYER_KEY = "ComputerPlayer";

  static final int TOP_LEFT = 0;
  static final int TOP_MIDDLE = 1;
  static final int TOP_RIGHT = 2;
  static final int MIDDLE_LEFT = 3;
  static final int CENTER = 4;
  static final int MIDDLE_RIGHT = 5;
  static final int BOTTOM_LEFT = 6;
  static final int BOTTOM_MIDDLE = 7;
  static final int BOTTOM_RIGHT = 8;

  static final int NO_MOVE = -1;

  static final String NAME = "name";
  static final int NAME_TEXT_SIZE = 22;
  static final int REMOVE_PLAYER_TEXT_SIZE = 18;
  static final int X_O_TEXT_SIZE = 50;
  static final int X_O_TEXT_SIZE_PHONE = 40;

  static final String NEW_USER_FRAGMENT = NewUserFragment.class.getCanonicalName();
  static final String MAIN_FRAGMENT = MainFragment.class.getCanonicalName();
  static final String STATS_FRAGMENT = StatsFragment.class.getCanonicalName();

  static final String USERS_KEY = "users";

  static final int STATS_TEXT_SIZE = 35;
  static final int STATS_LIST_TEXT_SIZE = 25;

  static final String WINS_KEY = "wins";
  static final String LOSSES_KEY = "losses";
  static final String TIES_KEY = "ties";
  static final String CURRENT_USER_KEY = "currentUser";
  static final String IS_NEW_GAME_KEY = "isNewGame";
  static final String CURRENT_PLAYER_KEY = "firstPlayer";
  static final String NAME_KEY = "name";

  static final Map<Player, Integer> COLORS;
  static {
    COLORS = new EnumMap<Player, Integer>(Player.class);
    COLORS.put(Player.COMPUTER, Color.RED);
    COLORS.put(Player.PLAYER, Color.BLUE);
  }
  static final int COMPUTER_SLEEP_MILLIS = 500;
  static final int LETS_PLAY_TEXT_SIZE = 50;
  static final int LETS_PLAY_TEXT_SIZE_JA = 40;
  static final int LETS_PLAY_TEXT_SIZE_JA_LANDSCAPE = 30;
  static final int LETS_PLAY_TEXT_SIZE_CN_LANDSCAPE = 35;

  static final String GAME_TYPE_KEY = "gameType";

}
