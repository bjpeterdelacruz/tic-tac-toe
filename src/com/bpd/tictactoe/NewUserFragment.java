package com.bpd.tictactoe;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.bpd.tictactoe.BoardManager.Player;

public class NewUserFragment extends Fragment {

  private MainActivity activity;
  private Resources res;
  private String selectedName;
  private boolean isRadioButtonSelected;

  private EditText nameEt;
  private Button newGameButton;

  private List<String> users;

  private RadioButton classicBtn, fourBtn, gomokuBtn;
  private GameType gameType;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    activity = (MainActivity) getActivity();
    res = activity.getResources();
    DatabaseHelper helper = null;
    users = new ArrayList<String>();
    try {
      helper = new DatabaseHelper(activity);
      helper.createDatabase();
      helper.openDatabase();
      helper.upgradeDatabase();
      users = helper.getAllUsers();
    }
    catch (Exception e) {
      Log.e(MainActivity.class.getCanonicalName(), e.getMessage(), e);
    }
    finally {
      if (helper != null) {
        helper.close();
      }
    }
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    String name = nameEt.getText().toString();
    outState.putString(Constants.NAME_KEY, name.length() == 0 ? selectedName : name);
    outState.putSerializable(Constants.GAME_TYPE_KEY, gameType);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    boolean isLandscapeOrientation = res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

    LinearLayout phoneLandscapeLayout = new LinearLayout(activity);
    LinearLayout.LayoutParams params =
        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    phoneLandscapeLayout.setLayoutParams(params);

    LinearLayout rightPanel = new LinearLayout(activity);
    rightPanel.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));

    LinearLayout layout = new LinearLayout(activity);
    layout.setOrientation(LinearLayout.VERTICAL);
    layout.setGravity(Gravity.CENTER);
    if (isLandscapeOrientation) {
      layout.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
      phoneLandscapeLayout.addView(layout);
      phoneLandscapeLayout.addView(rightPanel);
    }
    else {
      layout.setLayoutParams(params);
    }

    LinearLayout buttonsLayout = new LinearLayout(activity);
    buttonsLayout.setOrientation(LinearLayout.VERTICAL);
    final List<RadioButton> buttons = new ArrayList<RadioButton>();
    for (final String user : users) {
      RelativeLayout row = new RelativeLayout(activity);
      RelativeLayout.LayoutParams rowParams =
          new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
      rowParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
      rowParams.leftMargin = 35;

      final RadioButton rb = new RadioButton(activity);
      rb.setLayoutParams(rowParams);
      rb.setTextSize(Constants.NAME_TEXT_SIZE);
      rb.setText(user);
      rb.setOnClickListener(new OnClickListener() {

        @Override
        public void onClick(View v) {
          selectedName = rb.getText().toString();
          isRadioButtonSelected = true;
          newGameButton.setEnabled(true);
          for (RadioButton button : buttons) {
            button.setChecked(button.getText().toString().equals(selectedName));
          }
        }

      });
      row.addView(rb);
      buttons.add(rb);

      rowParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
      rowParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
      rowParams.addRule(RelativeLayout.CENTER_IN_PARENT);
      rowParams.rightMargin = 35;

      TextView removePlayerTv = new TextView(activity);
      removePlayerTv.setLayoutParams(rowParams);
      SpannableString spanString = new SpannableString(res.getString(R.string.remove_player));
      spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
      removePlayerTv.setText(spanString);
      removePlayerTv.setTextColor(Constants.COLORS.get(Player.COMPUTER));
      removePlayerTv.setTextSize(Constants.REMOVE_PLAYER_TEXT_SIZE);
      removePlayerTv.setOnClickListener(new OnClickListener() {

        @Override
        public void onClick(View v) {
          AlertDialog.Builder builder = new AlertDialog.Builder(activity);
          String msg;
          if (Locale.TRADITIONAL_CHINESE.equals(res.getConfiguration().locale)) {
            msg = res.getString(R.string.confirm_remove_player);
          }
          else if (Locale.JAPAN.equals(res.getConfiguration().locale)) {
            msg = String.format("%s%s", user, res.getString(R.string.confirm_remove_player));
          }
          else {
            msg = String.format("%s %s?", res.getString(R.string.confirm_remove_player), user);
          }
          final AlertDialog dialog =
              builder.setCancelable(false).setTitle(res.getString(R.string.remove_player)).setMessage(msg)
                  .setPositiveButton(res.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      removePlayer(user);
                    }
                  }).setNegativeButton(res.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      dialog.dismiss();
                    }
                  }).create();
          dialog.show();
        }

      });
      if (activity.isPhone() && isLandscapeOrientation) {
        buttonsLayout.addView(row);

        row = new RelativeLayout(activity);
        row.addView(removePlayerTv);
        buttonsLayout.addView(row);
      }
      else {
        row.addView(removePlayerTv);

        buttonsLayout.addView(row);
      }
    }
    ScrollView scrollView = new ScrollView(activity);
    scrollView.addView(buttonsLayout);

    nameEt = new EditText(activity);
    nameEt.setHint(res.getString(R.string.name));
    nameEt.setTextSize(Constants.NAME_TEXT_SIZE);
    nameEt.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.WRAP_CONTENT));
    nameEt.setSingleLine(true);
    nameEt.setImeOptions(EditorInfo.IME_ACTION_DONE);

    nameEt.addTextChangedListener(new TextWatcher() {

      @Override
      public void afterTextChanged(Editable s) {
        selectedName = nameEt.getText().toString();
        for (RadioButton rb : buttons) {
          rb.setEnabled(selectedName.length() == 0);
        }
        boolean isSameName = false;
        if (selectedName.length() > 0) {
          for (RadioButton rb : buttons) {
            if (rb.getText().toString().equals(selectedName)) {
              isSameName = true;
              break;
            }
          }
        }
        newGameButton.setEnabled((selectedName.length() > 0 || isRadioButtonSelected) && !isSameName);
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

    });

    GradientDrawable gd = new GradientDrawable();
    gd.setColor(Color.TRANSPARENT);
    gd.setCornerRadius(5);
    gd.setStroke(1, 0xFF000000);

    newGameButton = new Button(activity);
    newGameButton.setEnabled(false);
    newGameButton.setText(res.getString(R.string.new_game));
    newGameButton.setBackgroundDrawable(gd);
    newGameButton.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View view) {
        Bundle args = new Bundle();
        args.putString(Constants.CURRENT_USER_KEY, selectedName);
        if (!users.contains(selectedName)) {
          new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
              DatabaseHelper helper = null;
              try {
                helper = new DatabaseHelper(activity);
                helper.openDatabase();
                helper.insertUser(selectedName);
              }
              catch (SQLException sqle) {
                Log.e(NewUserFragment.class.getCanonicalName(), sqle.getMessage(), sqle);
              }
              finally {
                if (helper != null) {
                  helper.close();
                }
              }
              return null;
            }

          }.execute();
        }

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(nameEt.getWindowToken(), 0);

        BoardManager boardManager = new BoardManager(gameType);
        args.putSerializable(Constants.BOARD_MANAGER_KEY, boardManager);
        boardManager.setCurrentPlayer(BoardManager.getFirstPlayer());
        args.putSerializable(Constants.CURRENT_PLAYER_KEY, boardManager.getCurrentPlayer());
        args.putSerializable(Constants.COMPUTER_PLAYER_KEY, new ComputerPlayer(boardManager));

        Fragment fragment = new MainFragment();
        fragment.setArguments(args);
        FragmentManager manager = activity.getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.fragment_container, fragment, Constants.MAIN_FRAGMENT).commit();
      }

    });

    params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    params.topMargin = isLandscapeOrientation ? 25 : 50;

    TextView welcomeTv = new TextView(activity);
    welcomeTv.setLayoutParams(params);
    welcomeTv.setText(res.getString(R.string.lets_play));
    if (Locale.JAPAN.equals(res.getConfiguration().locale)) {
      if (isLandscapeOrientation) {
        welcomeTv.setTextSize(Constants.LETS_PLAY_TEXT_SIZE_JA_LANDSCAPE);
      }
      else {
        welcomeTv.setTextSize(Constants.LETS_PLAY_TEXT_SIZE_JA);
      }
    }
    else if (Locale.TAIWAN.equals(res.getConfiguration().locale)) {
      if (isLandscapeOrientation) {
        welcomeTv.setTextSize(Constants.LETS_PLAY_TEXT_SIZE_CN_LANDSCAPE);
      }
      else {
        welcomeTv.setTextSize(Constants.LETS_PLAY_TEXT_SIZE);
      }
    }
    else {
      welcomeTv.setTextSize(Constants.LETS_PLAY_TEXT_SIZE);
    }
    welcomeTv.setGravity(Gravity.CENTER);
    layout.addView(welcomeTv);

    LinearLayout row = new LinearLayout(activity);
    params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    params.topMargin = isLandscapeOrientation ? 25 : 100;
    params.leftMargin = 35;
    params.rightMargin = 35;
    params.bottomMargin = 25;
    row.setLayoutParams(params);

    nameEt.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 2.0f));
    row.addView(nameEt);

    if (!isLandscapeOrientation) {
      newGameButton.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
      row.addView(newGameButton);
    }

    layout.addView(row);

    classicBtn = new RadioButton(activity);
    classicBtn.setText(res.getString(R.string.classic));
    classicBtn.setTag(GameType.CLASSIC);
    classicBtn.setOnClickListener(new GameTypeButtonListener(classicBtn));
    if (!isLandscapeOrientation) {
      params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
      params.leftMargin = 35;
      classicBtn.setLayoutParams(params);
    }

    fourBtn = new RadioButton(activity);
    fourBtn.setText(res.getString(R.string.four));
    fourBtn.setTag(GameType.FOUR_IN_A_ROW);
    fourBtn.setOnClickListener(new GameTypeButtonListener(fourBtn));
    if (!isLandscapeOrientation) {
      fourBtn.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
    }

    gomokuBtn = new RadioButton(activity);
    gomokuBtn.setText(res.getString(R.string.gomoku));
    gomokuBtn.setTag(GameType.GOMOKU);
    gomokuBtn.setOnClickListener(new GameTypeButtonListener(gomokuBtn));
    if (!isLandscapeOrientation) {
      params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      params.rightMargin = 35;
      params.gravity = Gravity.RIGHT;
      gomokuBtn.setLayoutParams(params);
    }

    LinearLayout gameTypeButtonsRow = new LinearLayout(activity);
    params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    params.bottomMargin = 25;
    gameTypeButtonsRow.setLayoutParams(params);
    if (activity.isPhone() && res.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
      gameTypeButtonsRow.setOrientation(LinearLayout.VERTICAL);

      row = new LinearLayout(activity);
      params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      params.rightMargin = 35;
      row.setLayoutParams(params);
      row.addView(classicBtn);
      row.addView(fourBtn);
      gameTypeButtonsRow.addView(row);

      params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      params.leftMargin = 35;
      gomokuBtn.setLayoutParams(params);
      gameTypeButtonsRow.addView(gomokuBtn);
    }
    else {
      if (isLandscapeOrientation) {
        params =
            new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        gameTypeButtonsRow = new LinearLayout(activity);
        gameTypeButtonsRow.setLayoutParams(params);
        gameTypeButtonsRow.setOrientation(LinearLayout.VERTICAL);
        params.leftMargin = 35;
        classicBtn.setLayoutParams(params);
        fourBtn.setLayoutParams(params);
        gomokuBtn.setLayoutParams(params);
      }
      gameTypeButtonsRow.addView(classicBtn);
      gameTypeButtonsRow.addView(fourBtn);
      gameTypeButtonsRow.addView(gomokuBtn);
    }
    layout.addView(gameTypeButtonsRow);

    if (isLandscapeOrientation) {
      params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      params.topMargin = 35;
      params.leftMargin = 35;
      params.rightMargin = 35;
      params.bottomMargin = 35;
      newGameButton.setLayoutParams(params);
      layout.addView(newGameButton);
      rightPanel.addView(scrollView);
    }
    else {
      layout.addView(scrollView);
    }

    if (savedInstanceState != null) {
      String name = savedInstanceState.getString(Constants.NAME_KEY);
      boolean isChecked = false;
      for (RadioButton rb : buttons) {
        if (rb.getText().toString().equals(name)) {
          rb.setChecked(true);
          newGameButton.setEnabled(true);
          isChecked = true;
          break;
        }
      }
      if (!isChecked && name != null && name.length() > 0) {
        nameEt.setText(name);
      }
    }

    String key = Constants.GAME_TYPE_KEY;
    if (savedInstanceState == null) {
      int size = PreferenceManager.getDefaultSharedPreferences(activity).getInt(key, GameType.CLASSIC.getSize());
      gameType = GameType.getGameTypeForSize(size);
    }
    else {
      gameType = (GameType) savedInstanceState.getSerializable(Constants.GAME_TYPE_KEY);
    }
    switch (gameType) {
    case CLASSIC:
      classicBtn.setChecked(true);
      break;
    case FOUR_IN_A_ROW:
      fourBtn.setChecked(true);
      break;
    case GOMOKU:
      gomokuBtn.setChecked(true);
      break;
    default:
      throw new IllegalArgumentException("unsupported game type: " + gameType);
    }

    return isLandscapeOrientation ? phoneLandscapeLayout : layout;
  }

  private class GameTypeButtonListener implements OnClickListener {

    private final RadioButton button;

    public GameTypeButtonListener(RadioButton button) {
      this.button = button;
    }

    @Override
    public void onClick(View v) {
      gameType = (GameType) button.getTag();
      Editor editor = PreferenceManager.getDefaultSharedPreferences(activity).edit();
      editor.putInt(Constants.GAME_TYPE_KEY, gameType.getSize()).commit();
      classicBtn.setChecked(classicBtn.getTag().equals(gameType));
      fourBtn.setChecked(fourBtn.getTag().equals(gameType));
      gomokuBtn.setChecked(gomokuBtn.getTag().equals(gameType));
    }

  }

  private void removePlayer(final String user) {
    new AsyncTask<Void, Void, Void>() {

      @Override
      protected Void doInBackground(Void... params) {
        DatabaseHelper helper = null;
        try {
          helper = new DatabaseHelper(activity);
          helper.openDatabase();
          helper.removeUser(user);
        }
        catch (SQLException sqle) {
          Log.e(NewUserFragment.class.getCanonicalName(), sqle.getMessage(), sqle);
        }
        finally {
          if (helper != null) {
            helper.close();
          }
        }
        return null;
      }

      @Override
      protected void onPostExecute(Void results) {
        Fragment fragment = new NewUserFragment();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
      }

    }.execute();
  }

}
