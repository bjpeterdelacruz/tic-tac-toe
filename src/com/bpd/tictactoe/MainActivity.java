package com.bpd.tictactoe;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;

public class MainActivity extends FragmentActivity {

  private boolean isPhone;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_activity);

    isPhone = !isTablet(6);

    if (findViewById(R.id.fragment_container) != null && savedInstanceState != null) {
      return;
    }

    Fragment fragment = new NewUserFragment();
    FragmentManager manager = getSupportFragmentManager();
    manager.beginTransaction().add(R.id.fragment_container, fragment, Constants.NEW_USER_FRAGMENT).commit();
  }

  @Override
  public void onDestroy() {
    try {
      PreferenceManager.getDefaultSharedPreferences(this).edit().remove(Constants.GAME_TYPE_KEY).commit();
    }
    catch (Exception e) {
      Log.e(MainActivity.class.getCanonicalName(), e.getMessage(), e);
    }
    super.onDestroy();
  }

  @Override
  public void onBackPressed() {
    Fragment fragment = getSupportFragmentManager().findFragmentByTag(Constants.STATS_FRAGMENT);
    if (fragment != null) {
      getSupportFragmentManager().popBackStack();
      return;
    }
    fragment = getSupportFragmentManager().findFragmentByTag(Constants.MAIN_FRAGMENT);
    if (fragment != null) {
      ((MainFragment) fragment).updateStats();
      Fragment newFragment = new NewUserFragment();
      newFragment.setArguments(fragment.getArguments());
      getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment).commit();
      return;
    }
    super.onBackPressed();
  }

  boolean isPhone() {
    return isPhone;
  }

  private boolean isTablet(int inch) {
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    int widthPixels = metrics.widthPixels;
    int heightPixels = metrics.heightPixels;
    float widthDpi = metrics.xdpi;
    float heightDpi = metrics.ydpi;
    float widthInches = widthPixels / widthDpi;
    float heightInches = heightPixels / heightDpi;
    double diagonalInches = Math.sqrt((widthInches * widthInches) + (heightInches * heightInches));
    return (int) Math.round(diagonalInches) >= inch;
  }

}
