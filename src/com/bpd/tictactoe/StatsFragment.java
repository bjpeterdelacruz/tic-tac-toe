package com.bpd.tictactoe;

import android.os.Bundle;
import android.support.v4.app.ListFragment;

public class StatsFragment extends ListFragment {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setListAdapter(new StatsAdapter(getActivity()));
  }

}
